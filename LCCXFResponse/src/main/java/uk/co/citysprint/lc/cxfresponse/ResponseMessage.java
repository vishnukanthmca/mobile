package uk.co.citysprint.lc.cxfresponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class ResponseMessage
{

	//private String key;
	private String messageKey;

	private List<MessageParam> argList = new ArrayList<MessageParam>();

	public ResponseMessage(String key)
	{
		this.messageKey = key;
	}

	public List<MessageParam> getArgList()
	{
		return argList;
	}

	public void setArgList(List<MessageParam> argList)
	{
		this.argList = argList;
	}

	public void appendParam(String param)
	{
		MessageParam mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param);
		argList.add(mArg);
	}

	/**
	 * Appends the specified element to the end of the list.
	 * 
	 * @param param1
	 * @param param2
	 */
	public void appendParam(String param1, String param2)
	{
		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param1);
		argList.add(mArg);

		mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param2);
		argList.add(mArg);

	}

	/**
	 * Appends the specified element to the end of the list.
	 * 
	 * @param param1
	 * @param param2
	 * @param param3
	 */
	public void appendParam(String param1, String param2, String param3)
	{

		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param1);
		argList.add(mArg);

		mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param2);
		argList.add(mArg);

		mArg = new MessageParam();
		mArg.setStrClass(String.class.getName());
		mArg.setValue(param3);
		argList.add(mArg);
	}

	/**
	 * Appends specified elements to the end of the list.
	 */
	public void appendParam(String... vaParams)
	{
		MessageParam mArg;
		for (String param : vaParams)
		{
			mArg = new MessageParam();
			mArg.setStrClass(String.class.getName());
			mArg.setValue(param);
			argList.add(mArg);
		}
	}

	public void appendParam(Date param)
	{
		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(Date.class.getName());
		mArg.setValue(param);
		argList.add(mArg);

	}

	public void appendParam(int param)
	{
		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(Integer.class.getName());
		mArg.setValue(param);
		argList.add(mArg);
	}

	public void appendParam(boolean param)
	{
		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(Boolean.class.getName());
		mArg.setValue(new Boolean(param));
		argList.add(mArg);
	}

	public void appendParam(float param)
	{
		MessageParam mArg;

		mArg = new MessageParam();
		mArg.setStrClass(Float.class.getName());
		mArg.setValue(param);
		argList.add(mArg);
	}

	public String getMessageKey()
	{
		return messageKey;
	}

	public void setMessageKey(String messageKey)
	{
		this.messageKey = messageKey;
	}

	@Override
	public String toString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("(KEY: " + messageKey);
		if (argList != null && argList.size() > 0)
		{
			stringBuilder.append("[");
			for (MessageParam messageParam : argList)
			{
				stringBuilder.append("{");
				stringBuilder.append(messageParam.getStrClass());
				stringBuilder.append(",");
				stringBuilder.append(messageParam.getValue());
				stringBuilder.append("}");
				stringBuilder.append(" ");
			}
			stringBuilder.append("])");
		}
		return stringBuilder.toString();
	}

	public Object[] getParamValueArray()
	{
		if (argList == null || argList.size() == 0) return new Object[0];

		List<Object> valList = new ArrayList<Object>();

		Object value;
		String strClass;
		Date dateValue;

		for (MessageParam messageParam : argList)
		{
			strClass = StringUtils.stripToEmpty(messageParam.getStrClass());
			value = messageParam.getValue();

			if (strClass.endsWith("Date"))
			{

				if (value instanceof Number)
				{
					dateValue = new Date(((Number) value).longValue());
					valList.add(dateValue);
				}
				else
				{
					dateValue = new Date(Long.MAX_VALUE);
					valList.add(dateValue);
				}
			}
//			else if (strClass.endsWith("Boolean"))
//			{
//				valList.add(new Boolean((Boolean) value));
//			}
			else
			{
				valList.add(value);
			}
		}

		return valList.toArray(new Object[0]);

	}
}// class
