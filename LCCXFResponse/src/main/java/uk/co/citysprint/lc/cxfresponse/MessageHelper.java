package uk.co.citysprint.lc.cxfresponse;

import java.util.ArrayList;
import java.util.List;

public class MessageHelper
{

	public static ResponseMessage getResponseMessage(String key)
	{
		ResponseMessage rm = new ResponseMessage(key);
		return rm;
	}

	public static List<ResponseMessage> getResponseMessageList(String key)
	{
		ResponseMessage rm = getResponseMessage(key);
		List<ResponseMessage> lrm = new ArrayList<ResponseMessage>();
		lrm.add(rm);
		return lrm;
	}

	public static List<ResponseMessage> getResponseMessageList(ResponseMessage rm)
	{
		List<ResponseMessage> lrm = new ArrayList<ResponseMessage>();
		lrm.add(rm);
		return lrm;
	}

	/**
	 * getOneMessage preferred over getAMessage.
	 * @param key
	 * @return
	 */
	public static List<ResponseMessage> getOneMessage(String key)
	{
		return getResponseMessageList(key);
	}

	public static List<ResponseMessage> getOneMessage(String key, String param)
	{
		ResponseMessage rm = getResponseMessage(key);
		rm.appendParam(param);
		return getResponseMessageList(rm);
	}

	public static List<ResponseMessage> getOneMessage(String key, String param,
	        String param2)
	{
		ResponseMessage rm = getResponseMessage(key);
		rm.appendParam(param);
		rm.appendParam(param2);
		return getResponseMessageList(rm);
	}

	public static List<ResponseMessage> getTwoMessages(String message1Key,
	        String message2Key)
	{

		List<ResponseMessage> ret = getResponseMessageList(getResponseMessage(message1Key));
		ret.add(getResponseMessage(message2Key));
		return ret;
	}
	
	public static List<ResponseMessage> getMultipleMessages(String message1Key,
			String message2Key, String message3Key)
	{

		List<ResponseMessage> ret = getResponseMessageList(getResponseMessage(message1Key));
		ret.add(getResponseMessage(message2Key));
		ret.add(getResponseMessage(message3Key));
		return ret;
	}

}// class
