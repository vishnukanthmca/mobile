package uk.co.citysprint.lc.model;

public class Config {

	private String hosts;    
	private String bucketName;    
    private String bucketPasswd;
    
    public String getHosts() {
		return hosts;
	}
	public void setHosts(String hosts) {
		this.hosts = hosts;
	}
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	public String getBucketPasswd() {
		return bucketPasswd;
	}
	public void setBucketPasswd(String bucketPasswd) {
		this.bucketPasswd = bucketPasswd;
	}

}
