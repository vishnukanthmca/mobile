package uk.co.citysprint.lc.couchbase.base;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.CouchbaseTemplate;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import uk.co.citysprint.lc.model.Config;

import java.util.Arrays;
import java.util.List;

/**
 * Annotation-based Configuration ("JavaConfig")
 */
@Configuration
@EnableCouchbaseRepositories("uk.co.citysprint.lc.couchbase.base")
public class Template extends AbstractCouchbaseConfiguration {

    private String hosts;    
    private String bucketName;    
    private String bucketPasswd;
    
    public static Template connection = null;
    
    public Template(){
    	
    }
    
    public Template(Config config){
    	hosts = config.getHosts();
    	bucketName = config.getBucketName();
    	bucketPasswd = config.getBucketPasswd();
    }

    @Override
    protected String getBucketName() {
        
    	return this.bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return this.bucketPasswd;
    }

    private static Template getInstance(Config config){    	
    	if(null == connection){
    		synchronized(Template.class) {
    			if(null == connection){
    				connection = new Template(config);
    			}
    		}
    	}
    	return connection;
    }
    
    public static CouchbaseTemplate couchbaseTemplate(Config config) throws Exception{
    	return Template.getInstance(config).couchbaseTemplate();
    }

	@Override
	protected List<String> getBootstrapHosts() {
		return Arrays.asList(this.hosts.split(","));
	}
	
	public String getHosts() {
		return hosts;
	}

	public void setHosts(String hosts) {
		this.hosts = hosts;
	}

	public String getBucketPasswd() {
		return bucketPasswd;
	}

	public void setBucketPasswd(String bucketPasswd) {
		this.bucketPasswd = bucketPasswd;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
}