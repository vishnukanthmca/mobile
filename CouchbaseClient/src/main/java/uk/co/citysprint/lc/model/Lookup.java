package uk.co.citysprint.lc.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry = 0)
public class Lookup {

	@Id
    private String id;
	
	@Field
    private String lookupDocumentId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLookupDocumentId() {
		return lookupDocumentId;
	}

	public void setLookupDocumentId(String lookupDocumentId) {
		this.lookupDocumentId = lookupDocumentId;
	}

	public Lookup(String id, String lookupDocumentId){
		this.id = id;
		this.lookupDocumentId = lookupDocumentId;
	}
	
}
