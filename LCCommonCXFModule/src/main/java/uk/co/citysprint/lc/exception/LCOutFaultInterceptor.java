package uk.co.citysprint.lc.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.json.simple.JSONObject;

import uk.co.citysprint.lc.service.IConstants;

public class LCOutFaultInterceptor extends AbstractPhaseInterceptor<Message> {
    private boolean handleMessageCalled;
    public LCOutFaultInterceptor() {
        this(Phase.PRE_STREAM);
    }

    public LCOutFaultInterceptor(String s) {
        super(Phase.MARSHAL);
        
    } 

    public void handleMessage(Message message) throws Fault {
        //System.out.println("Out Fault Interceptor---");
        handleMessageCalled = true;
        Exception ex = message.getContent(Exception.class);
        if (ex == null) {
            throw new RuntimeException("Exception is expected");
        }
        if (!(ex instanceof Fault)) {
            throw new RuntimeException("Fault is expected");
        }
        // deal with the actual exception : fault.getCause()
        HttpServletResponse response = (HttpServletResponse)message.getExchange()
            .getInMessage().get(AbstractHTTPDestination.HTTP_RESPONSE);
        response.setStatus(500);
        response.setContentType("application/json");
        try {
            response.getOutputStream().write(jsonFormat(IConstants.INTERNAL_SERVER_ERROR).getBytes());
            response.getOutputStream().flush();
            message.getInterceptorChain().abort();    
        } catch (IOException ioex) {
            throw new RuntimeException(IConstants.IOEXCEPTION);
        }
        
    }
    
    private String jsonFormat(String value){
    	JSONObject response = new JSONObject();
    	response.put("errorCode", value);   
    	response.put("errorMessage", "Internal server error");
    	return response.toString();
    }
     

    protected boolean handleMessageCalled() {
        return handleMessageCalled;
    }

}