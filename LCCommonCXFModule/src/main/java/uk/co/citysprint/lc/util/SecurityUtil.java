package uk.co.citysprint.lc.util;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Base64;


public class SecurityUtil {

	public static String encrypt(String value,String secretKey) throws Exception {
		BlowfishEngine engine = new BlowfishEngine();
		PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(engine);
		KeyParameter key = new KeyParameter(secretKey.getBytes());
		cipher.init(true, key);
		byte in[] = value.getBytes();
		byte out[] = new byte[cipher.getOutputSize(in.length)];
		int len1 = cipher.processBytes(in, 0, in.length, out, 0);
		try {
			cipher.doFinal(out, len1);
		} catch (CryptoException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		String s = new String(Base64.encode(out));
		return s;
	}
	
	public static String decrypt(String value,String secretKey) throws Exception {
		BlowfishEngine engine = new BlowfishEngine();
		PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(engine);
		StringBuffer result = new StringBuffer();
		KeyParameter key = new KeyParameter(secretKey.getBytes());
		cipher.init(false, key);
		byte out[] = Base64.decode(value);
		byte out2[] = new byte[cipher.getOutputSize(out.length)];
		int len2 = cipher.processBytes(out, 0, out.length, out2, 0);
		cipher.doFinal(out2, len2);
		String s2 = new String(out2);
		for (int i = 0; i < s2.length(); i++) {
			char c = s2.charAt(i);
			if (c != 0) {
				result.append(c);
			}
		}
		return result.toString();
	}
}
