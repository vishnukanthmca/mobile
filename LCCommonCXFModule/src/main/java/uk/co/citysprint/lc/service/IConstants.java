package uk.co.citysprint.lc.service;

public interface IConstants {
	
	public static final String INTERNAL_SERVER_ERROR="LC-EX-COMM-001";
	public static final String IOEXCEPTION = "LC-EX-IO-001";
	public static final String BADREQUEST = "LC-EX-COMM-005";
	public static final String USER_ALREADY_EXISTS = "LC-USER-COMM-001";
	public static final String PHONENUMBER_NOT_EXISTS = "LC-DATA-COMM-001";
}
