package uk.co.citysprint.lc.service;

import java.util.UUID;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.model.ResponseError;

/**
 * {@link AbstractServiceImpl} is the which extracts the uri info parameters of
 * the Rest request
 * 
 */
public class AbstractServiceImpl
{


	private @Context
	UriInfo uriInfo;
	
	private @Context
	HttpHeaders httpHeaders;
	

	/**
	 * Returns a MultivaluedMap of query parameters from the webservice call
	 * 
	 * @return MultivaluedMap<String, String> of queryparams
	 */
	public MultivaluedMap<String, String> getQueryParams()
	{
		return uriInfo.getQueryParameters();
	}

	/**
	 * Returns a MultivaluedMap of path parameters from the webservice call
	 * 
	 * @return MultivaluedMap<String, String> of path params
	 */
	public MultivaluedMap<String, String> getPathParams()
	{
		return uriInfo.getPathParameters();
	}

	public Response getResponseError(Status statusCode, String messageCode)
	{
		ResponseError error = new ResponseError();		
		error.setErrorCode(messageCode);
		return Response.status(statusCode).entity(error).build();
	}
	
	public Response getResponseSuccess(Object entity)
	{		
		return Response.status(Response.Status.OK).entity(entity).build();
	}
	
	public Object getRequestHeader(String name) throws LCException{
		Object header = null;
		try{			
			if(httpHeaders.getRequestHeader(name) == null) {
				throw new LCException("Header :"+name+" not provided in request header");
			}else {
				header = httpHeaders.getRequestHeader(name).get(0);
			}
		}catch(Exception e){
			header = "test";
		}
		return header;
	}

	public String getDocId(){
		return UUID.randomUUID().toString();
	}

}
