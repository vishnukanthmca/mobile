package uk.co.citysprint.lc.responsemediator;

public class Error {
	
	/**
	 * 
	 */
	private String errorCode;
	
	/**
	 * error description taken from Message properties for the error id
	 */
	private String errorMessage;
	
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
