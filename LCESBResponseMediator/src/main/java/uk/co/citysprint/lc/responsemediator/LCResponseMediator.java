package uk.co.citysprint.lc.responsemediator;

import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.Mediator;
import org.apache.synapse.MessageContext;

import uk.co.citysprint.lc.cxfresponse.ResponseMessage;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class LCResponseMediator implements Mediator
{

	private static final Log LOGGER = LogFactory
	        .getLog(LCResponseMediator.class);

	private static final String JSON_PAYLOAD_PROPERTY = "JSONPayLoad";

	private static final String MESSAGE_FILE_BASE_NAME = "MessageBundle";

	private static final String TRANSFORMED_MESSAGE_KEY = "OMNI_TRANSFORMED_MESSAGE";

	private static final String TRANSFORMED_ERROR_MESSAGE = "LC_ERROR_MESSAGE";

	private static final String LOCALE_PROP_KEY = "locale";

	private static final String DEFAULT_LOCALE = "en";

	private static final Type LIST_TYPE = new TypeToken<List<ResponseMessage>>()
	{
	}.getType();

	/**
	 * A property named JSONPayLoad must be set before this is called. It must
	 * contain the JSONPayload of the response. This way we can avoid XML to
	 * JSON transformation. Also, we depend on WSO2 for the transformation.
	 * Hence we do not need to introduce our libraries.<br>
	 * <br>
	 * 
	 * Converts/ Transforms JSON to Java Object tree. Uses GSON to do so.<br>
	 * <br>
	 * 
	 * Locale - A property named locale must be set from outside which will be
	 * used to create a new Locale object.<br>
	 * <br>
	 * 
	 * Loads ResourceBundle. Base name is MessageBundle. Caching of bundles is
	 * provided by ResourceBundle by default.<br>
	 * 
	 * Iterates thru' List<ResponseMessage>. For each ResponseMessage, fetches
	 * the template for the key and formats the template by supplying
	 * parameters, if any.<br>
	 * <br>
	 * 
	 * Collects formatted messages. Uses GSON (gson.toJson(...)) to convert Java
	 * Objects to JSON.<br>
	 * Converted JSON is set as OMNI_TRANSFORMED_MESSAGE property.<br>
	 * <br>
	 * 
	 */

	@Override
	public boolean mediate(MessageContext mc)
	{
		// TODO Auto-generated method stub
		List<Error> listOfErrors = new ArrayList<Error>(1);
		try
		{
			debug("~~~~~~~~~~~~~~~~~~~~ LCMessageMediator");

			String jsonPayLoad = StringUtils.stripToEmpty((String) mc
			        .getProperty(JSON_PAYLOAD_PROPERTY));

			debug(JSON_PAYLOAD_PROPERTY + jsonPayLoad);

			if (StringUtils.isEmpty(jsonPayLoad))
			{
				debug("Property " + JSON_PAYLOAD_PROPERTY
				        + " is empty. Nothing to do here. Returning.");
				return true; // nothing to do.
			}

			Gson gson = new Gson();

			debug("About to transform JSON to Java Objects.");

			List<ResponseMessage> responseMessageList = null;
			Iterator<ResponseMessage> responseMessageItr;
			ResponseMessage responseMessage;

			try
			{
				responseMessageList = gson.fromJson(jsonPayLoad, LIST_TYPE);

				// Gson doesn't have a JSON schema validation feature... There
				// are many ways to implement schema validation.
				// This is one such.
				// https://sites.google.com/site/gson/gson-user-guide
				// "While deserialization, a missing entry in JSON results in setting the corresponding field in the object to null"

				responseMessageItr = responseMessageList.iterator();
				while (responseMessageItr.hasNext())
				{
					responseMessage = responseMessageItr.next();
					if (responseMessage.getMessageKey() == null)
					{
						debug("I don't like this JSON. Message key is null. LCResponseMediator will not process further.");
						return true;
					}

				}// while

			}
			catch (Exception ex)
			{
				debug("I don't like this JSON. LCResponseMediator will not process further. "
				        + ex.getMessage());
				return true;
			}

			debug("Transformed JSON to Java Objects. Message list size is "
			        + responseMessageList.size());

			String strLocale = DEFAULT_LOCALE;

			strLocale = StringUtils.stripToEmpty((String) mc
			        .getProperty(LOCALE_PROP_KEY));

			if (StringUtils.isEmpty(strLocale)) strLocale = DEFAULT_LOCALE;

			debug("Locale is " + strLocale);

			Locale locale = new Locale(strLocale);

			debug("MESSAGE_FILE_BASE_NAME " + MESSAGE_FILE_BASE_NAME);

			ResourceBundle resourceBundle = ResourceBundle.getBundle(
			        MESSAGE_FILE_BASE_NAME, locale);

			debug("No problem with message file.");
			
			
			LOGGER.info("Iteraating the message in Resource bundle");
			Enumeration<String> enums = resourceBundle.getKeys();
			LOGGER.info("Total messages in Resource bundle"+resourceBundle.keySet().size());
			LOGGER.info("Total messages in Resource bundle"+resourceBundle.keySet());

			List<String> collector = new ArrayList<String>();

			StringBuilder stringBuilder = new StringBuilder();

			MessageFormat formatter = new MessageFormat("", locale);

			debug("About to iterate.");

			responseMessageItr = responseMessageList.iterator();

			Error error;
			
			
			for (int i = 1; responseMessageItr.hasNext(); i++)
			{
				error = new Error();

				responseMessage = responseMessageItr.next();

				debug(i + " ResponseMessage " + responseMessage);

				stringBuilder.setLength(0);

				try
				{
					String key = StringUtils.stripToEmpty(responseMessage
					        .getMessageKey());
					error.setErrorCode(key);

					if (resourceBundle.containsKey(key))
					{
						String rbMessage = resourceBundle.getString(key);

						formatter.applyPattern(rbMessage);

						stringBuilder.append(formatter.format(responseMessage
						        .getParamValueArray()));
						error.setErrorMessage(formatter.format(responseMessage
						        .getParamValueArray()));
					}
					else
					{
						LOGGER.error("Key not found in ResourceBundle - " + key);
						stringBuilder.setLength(0);
						stringBuilder.append("No Key. ");
						stringBuilder.append(responseMessage);
					}

				}
				catch (Exception ex)
				{
					LOGGER.error("Error while resolving a message line.", ex);
					stringBuilder.setLength(0);
					stringBuilder.append(responseMessage);
				}
				listOfErrors.add(error);

				collector.add(stringBuilder.toString());

				debug(stringBuilder.toString());
			}
			if (!listOfErrors.isEmpty())
			{
				mc.setProperty(TRANSFORMED_ERROR_MESSAGE,
				        gson.toJson(listOfErrors.get(0)));
				debug("listOfErrors size is " + listOfErrors.size());
			}

			if (collector != null && collector.size() > 0)
			{
				mc.setProperty(TRANSFORMED_MESSAGE_KEY, gson.toJson(collector));

				debug("Collector size is " + collector.size());
			}

			debug("XXXXXXXXXXXXXXXXXXXXXXXXXX");
		}
		catch (Exception ex)
		{
			LOGGER.error("ERROR WHILE PARSING.", ex);
		}

		return true;

	}

	private void debug(Object msg)
	{
		if (!LOGGER.isDebugEnabled()) return;

		LOGGER.debug(msg);
	}

	@Override
	public void setMediatorPosition(int arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setShortDescription(String arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setTraceState(int arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public String getDescription()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDescription(String arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public int getMediatorPosition()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getShortDescription()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTraceState()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isContentAware()
	{
		// TODO Auto-generated method stub
		return false;
	}

}// class
