package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Consumes("application/json")
@Produces("application/json")
public interface CenterService {

	@POST
	@Path("/")
	public Response storeServiceCenters(String body);

}
