package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import org.apache.cxf.common.util.StringUtils;

import uk.co.citysprint.lc.couchbase.base.CouchTemplate;
import uk.co.citysprint.lc.cxfresponse.MessageHelper;
import uk.co.citysprint.lc.logging.LCLogger;
import uk.co.citysprint.lc.model.Lookup;
import uk.co.citysprint.lc.model.User;
import uk.co.citysprint.lc.service.AbstractServiceImpl;
import uk.co.citysprint.lc.service.IConstants;
import uk.co.citysprint.lc.util.SecurityUtil;

public class SignupServiceImpl extends AbstractServiceImpl implements SignupService{
	
	private static final LCLogger LOGGER = new LCLogger(SignupServiceImpl.class);
			
	private CouchTemplate<User> dbTemplateImpl;
	
	@Override
	public Response createUser(User user) throws Exception{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";		
		LOGGER.info(transId+"createUser method invoked");
		if(user == null || StringUtils.isEmpty(user.getUsername())){
			LOGGER.error(transId+"Error on receiving input");
			return Response.status(Response.Status.BAD_REQUEST).entity(
					MessageHelper.getOneMessage(IConstants.BADREQUEST)).build();
		}
		user.setId("um::"+getDocId());
		
			try{				
				boolean exists = this.dbTemplateImpl.exists(user.getUsername());
				if(exists){
					LOGGER.error(transId+"Error user already exists");
					return Response.status(Response.Status.BAD_REQUEST).entity(
							MessageHelper.getOneMessage(IConstants.USER_ALREADY_EXISTS)).build();
				}
				boolean idStatus = this.dbTemplateImpl.exists(user.getId());
				if(idStatus){
					user.setId("um::"+getDocId());
				}
				user.setPassword(SecurityUtil.encrypt(user.getPassword(), user.getId()));
				this.dbTemplateImpl.add(user);
				LOGGER.debug(transId+"user data is added successly");
				
				Lookup lookup = new Lookup(user.getUsername(),user.getId());
				this.dbTemplateImpl.addLocate(lookup);
				LOGGER.debug(transId+"user locate doc is added successly");
				
			} catch(NullPointerException e){
				LOGGER.error(transId+"Password is null: ");
				return Response.status(Response.Status.BAD_REQUEST).entity(
						MessageHelper.getOneMessage(IConstants.USER_ALREADY_EXISTS)).build();
			} catch(Exception e){
				LOGGER.error(transId+"Error caused on creating user documents:", e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
						MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
			}
			
		return getResponseSuccess(user);
	}
	
	public void remove(User user) throws Exception{
		this.dbTemplateImpl.remove(user);
	}
	
	public CouchTemplate<User> getDbTemplateImpl() {
		return dbTemplateImpl;
	}

	public void setDbTemplateImpl(CouchTemplate<User> dbTemplateImpl) {
		this.dbTemplateImpl = dbTemplateImpl;
	}
	


}
