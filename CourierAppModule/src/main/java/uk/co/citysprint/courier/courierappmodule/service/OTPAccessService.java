package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import uk.co.citysprint.courier.courierappmodule.model.OTPData;
import uk.co.citysprint.lc.exception.LCException;

@Consumes("application/json")
@Produces("application/json")
public interface OTPAccessService {
		
	@GET
	@Path("/")
	public Response getOTPAccess(String body) throws LCException;
	
	@POST
	@Path("/")
	public Response persistTransRef(OTPData opt) throws LCException;
}
