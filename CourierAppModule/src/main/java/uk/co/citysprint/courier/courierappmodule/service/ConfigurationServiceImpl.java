package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import uk.co.citysprint.lc.couchbase.base.CouchTemplate;
import uk.co.citysprint.lc.cxfresponse.MessageHelper;
import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.logging.LCLogger;
import uk.co.citysprint.lc.model.Configuration;
import uk.co.citysprint.lc.model.User;
import uk.co.citysprint.lc.service.AbstractServiceImpl;
import uk.co.citysprint.lc.service.IConstants;

public class ConfigurationServiceImpl extends AbstractServiceImpl implements ConfigurationService{

	private static final LCLogger LOGGER = new LCLogger(ConfigurationServiceImpl.class);
		
	private CouchTemplate<User> dbTemplateImpl;

	@Override
	public Response getConfigValue() throws LCException{
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";	
		LOGGER.info(transId + "getConfigValue method invoked");
				 
		Configuration config = null;
		try{
			config = (Configuration) this.dbTemplateImpl.get("configuration", Configuration.class);
		}catch(Exception e){
			LOGGER.error(transId + "Error caused on retriving configuration document:", e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
					MessageHelper.getOneMessage(IConstants.INTERNAL_SERVER_ERROR)).build();
		}
		
		return getResponseSuccess(config);
	}
	
	public CouchTemplate<User> getDbTemplateImpl() {
		return dbTemplateImpl;
	}

	public void setDbTemplateImpl(CouchTemplate<User> dbTemplateImpl) {
		this.dbTemplateImpl = dbTemplateImpl;
	}
}
