package uk.co.citysprint.courier.courierappmodule.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

@Document(expiry = 0)
public class OTPData {

	@Id
    private String id;
	
	@Field
    private String smsTransReference;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSmsTransReference() {
		return smsTransReference;
	}

	public void setSmsTransReference(String smsTransReference) {
		this.smsTransReference = smsTransReference;
	}

	public OTPData(){
		
	}
	
}
