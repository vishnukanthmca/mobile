package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import org.apache.cxf.common.util.StringUtils;

import uk.co.citysprint.courier.courierappmodule.model.OTPData;
import uk.co.citysprint.lc.couchbase.base.CouchTemplate;
import uk.co.citysprint.lc.cxfresponse.MessageHelper;
import uk.co.citysprint.lc.exception.LCException;
import uk.co.citysprint.lc.logging.LCLogger;
import uk.co.citysprint.lc.service.AbstractServiceImpl;
import uk.co.citysprint.lc.service.IConstants;

public class CenterServiceImpl extends AbstractServiceImpl implements
		CenterService {

	private static final LCLogger LOGGER = new LCLogger(CenterServiceImpl.class);

	private CouchTemplate<OTPData> dbTemplateImpl;

	public Response getOTPAccess(String phonenumber) throws LCException {
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";
		LOGGER.info(transId + "getOTPAccess method invoked");

		String phoneNumber = phonenumber;

		if (StringUtils.isEmpty(phonenumber)) {
			try {
				phoneNumber = getQueryParams().getFirst("phonenumber");
			} catch (Exception e) {
			}
		}

		LOGGER.debug(transId + "phoneNumber-" + phoneNumber);

		if (StringUtils.isEmpty(phoneNumber)) {
			LOGGER.error(transId + "Bad Request, phonenumber missing: "
					+ IConstants.BADREQUEST);
			return Response.status(Response.Status.BAD_REQUEST)
					.entity(MessageHelper.getOneMessage(IConstants.BADREQUEST))
					.build();
		}
		OTPData otp = null;
		try {
			otp = this.dbTemplateImpl.get(phoneNumber, OTPData.class);
		} catch (Exception e) {
			LOGGER.error(transId + "Error on retreving transRef details:", e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(MessageHelper
							.getOneMessage(IConstants.INTERNAL_SERVER_ERROR))
					.build();
		}

		if (otp == null) {
			LOGGER.error(transId + "Not found, phonenumber missing: "
					+ IConstants.PHONENUMBER_NOT_EXISTS);
			return Response
					.status(Response.Status.NOT_FOUND)
					.entity(MessageHelper
							.getOneMessage(IConstants.PHONENUMBER_NOT_EXISTS))
					.build();
		}

		return getResponseSuccess(otp);

	}

	public Response persistTransRef(OTPData transRef) throws LCException {
		String transId = (String) getRequestHeader("X-Client-Trans-Id") + ": ";
		LOGGER.info(transId + "persistTransRef method invoked");
		if (StringUtils.isEmpty(transRef.getId())
				|| StringUtils.isEmpty(transRef.getSmsTransReference())) {
			LOGGER.error(transId + "Error on parameters:");
			return Response.status(Response.Status.BAD_REQUEST)
					.entity(MessageHelper.getOneMessage(IConstants.BADREQUEST))
					.build();
		}
		try {
			this.dbTemplateImpl.save(transRef);
		} catch (Exception e) {
			LOGGER.error(transId + "Error on saving transRef details:", e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(MessageHelper
							.getOneMessage(IConstants.INTERNAL_SERVER_ERROR))
					.build();
		}
		return getResponseSuccess(transRef);
	}

	public void remove(OTPData data) throws Exception {
		this.dbTemplateImpl.remove(data);
	}

	public CouchTemplate<OTPData> getDbTemplateImpl() {
		return dbTemplateImpl;
	}

	public void setDbTemplateImpl(CouchTemplate<OTPData> dbTemplateImpl) {
		this.dbTemplateImpl = dbTemplateImpl;
	}

	@Override
	public Response storeServiceCenters(String body) {

		System.out.println("center service input body is " + body);

		OTPData data = new OTPData();
		data.setId("manualid");
		data.setSmsTransReference("sample body");
		return getResponseSuccess(data);
	}
}
