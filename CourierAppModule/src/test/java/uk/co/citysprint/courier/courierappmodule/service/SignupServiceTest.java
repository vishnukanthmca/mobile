package uk.co.citysprint.courier.courierappmodule.service;

import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import uk.co.citysprint.lc.model.User;

@ContextConfiguration(locations="common-context.xml")
@Ignore
public class SignupServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired private SignupServiceImpl signup;
	
	@Test (expected = ClassCastException.class)
	public void testNullInput() throws Exception{
		Response connect = signup.createUser(null);
		User user = (User) connect.getEntity();		
	}
	
	@Test (expected = ClassCastException.class)
	public void testEmptyRecord() throws Exception{
		Response connect = signup.createUser(new User());
		User user = (User) connect.getEntity();
	}
	
	@Test
	public void testUserSubmission() throws Exception{
		testRemoveUser("test@test.com");
		User user = new User();			
		user.setUsername("test@test.com");
		user.setPassword("ABCD123");
		Response connect = signup.createUser(user);
		user = (User) connect.getEntity();
		Assert.assertEquals("test@test.com", user.getUsername());	
		testRemoveUser(user.getId());
	}
	
	@Test (expected = ClassCastException.class)
	public void testUserSubmissionWithoutPassword() throws Exception{
		testRemoveUser("test@test.com");
		User user = new User();			
		user.setUsername("test@test.com");
		Response connect = signup.createUser(user);
		user = (User) connect.getEntity();
	}
	
	@Test (expected = ClassCastException.class)
	public void testUserSubmissionDuplicate() throws Exception{
		testRemoveUser("test@test.com");
		User user = new User();			
		user.setUsername("test@test.com");
		Response connect = signup.createUser(user);
		user = (User) connect.getEntity();
		Assert.assertEquals("test@test.com", user.getUsername());	
		testRemoveUser(user.getId());
		user = new User();			
		user.setUsername("test@test.com");
		connect = signup.createUser(user);
		user = (User) connect.getEntity();		
	}	
	
	
	private void testRemoveUser(String username){
		try{
			User user = new User(username);			
			signup.remove(user);
		}catch(Exception e){}
	}
}
